### installation of golang version manager
```
bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)
gvm install go1.14.1 -B
gvm list
gvm use go1.14.1
```

### initialization of default mongodb schema
```
go run bin/init/init_mongodb.go
```

### graphiql interface
```
http://localhost:8080/graphql

query signIn($login: String, $password: String) {
  signIn(login: $login, password: $password) {
    token
  }
}

{
  "login": "operator_pizza",
  "password": "operator_pizza"
}
```

### testing
```
go get github.com/smartystreets/goconvey
$GOPATH/bin/goconvey
http://localhost:8080
```
