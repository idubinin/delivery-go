package userpkg

import (
	"context"
	"goapp/models/usermodel"
)

type UseCase interface {
	// SignUp(ctx context.Context, username, password string) error
	SignIn(ctx context.Context, username, password string) (string, error)
	ParseToken(ctx context.Context, accessToken string) (*usermodel.UserSystem, error)
}
