package userpkg

import (
	"context"
	"goapp/models/usermodel"
)

type Repository interface {
	// CreateUser(ctx context.Context, user *usermodel.UserSystem) error
	GetUser(ctx context.Context, login, password string) (*usermodel.UserSystem, error)
}
