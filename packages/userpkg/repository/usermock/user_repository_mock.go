package usermock

import (
	"context"
	"goapp/bin/testdata"
	"goapp/models/usermodel"
)

// UserRepositoryMock
type UserRepositoryMock struct{}

// GetUser - get user by login and password
func (r UserRepositoryMock) GetUser(ctx context.Context, login, password string) (*usermodel.UserSystem, error) {
	umArr := testdata.GetTestUsers()
	um := usermodel.UserSystem{}

	for _, u := range umArr {
		if u.Login == login && u.Password == password {
			um = u
			break
		}
	}

	return &um, nil
}
