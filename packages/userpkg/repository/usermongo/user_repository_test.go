package usermongo_test

import (
	"context"
	"goapp/bin/testdata"
	"goapp/packages/userpkg/repository/usermongo"
	"goapp/utils"
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
)

const dbName string = "delivery_test_users_system"
const usrCollName string = "test_users_system"

var db *mongo.Database

func TestMain(m *testing.M) {

	// You create an Person and you save in database
	setUp()
	retCode := m.Run()

	// When you have executed the test, the Person is deleted from database
	tearDown()
	os.Exit(retCode)
}

func setUp() {
	viper.SetConfigType("json")
	viper.SetConfigName("config")
	viper.AddConfigPath("../../../../utils")
	viper.ReadInConfig()

	db = utils.MongoInit(dbName)
	testdata.InsertUsers(db, usrCollName)
}

func tearDown() {
	db.Drop(context.TODO())
}

func TestGetUser(t *testing.T) {
	userRepo := usermongo.NewUserRepository(db, usrCollName)
	um, err := userRepo.GetUser(context.TODO(), "courier_pizza", "a4a165fc8fca19f70e88fad37d5476ba1d0b7415")

	t.Log("um", um)

	assert.Nil(t, err)
	assert.NotNil(t, um)
	assert.Equal(t, (*um).Login, "courier_pizza")
	assert.Equal(t, (*um).ID, "5e874f4b327272d07e537a50")
}
