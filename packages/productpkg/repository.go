package productpkg

import (
	"context"
	"goapp/models/productmodel"
)

type Repository interface {
	GetProducts(ctx context.Context) ([]*productmodel.Product, error)
	GetProduct(ctx context.Context, id string) (productmodel.Product, error)
}
