package productcase

import (
	"context"
	"goapp/models/productmodel"
	"goapp/packages/productpkg"
)

type ProductCase struct {
	productRepo productpkg.Repository
}

func NewProductCase(productRepo productpkg.Repository) *ProductCase {
	return &ProductCase{
		productRepo: productRepo,
	}
}

func (pc ProductCase) GetProducts(ctx context.Context) ([]*productmodel.Product, error) {
	return pc.productRepo.GetProducts(ctx)
}

func (pc ProductCase) GetProduct(ctx context.Context, id string) (productmodel.Product, error) {
	if id == "" {
		return productmodel.Product{}, productpkg.ErrProductNotFound
	}
	return pc.productRepo.GetProduct(ctx, id)
}
