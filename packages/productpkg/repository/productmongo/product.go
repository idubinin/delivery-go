package productmongo

import (
	"goapp/models/productmodel"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Product is object of product for organisation
type Product struct {
	ID       primitive.ObjectID   `bson:"_id,omitempty"`
	IDCl     primitive.ObjectID   `bson:"_id_client"`
	IDsEst   []primitive.ObjectID `bson:"_ids_estb"`
	Name     string
	Category string
	Price    int
	Status   string
}

func toModel(p *Product) *productmodel.Product {
	IdsEst := []string{}
	for _, id := range p.IDsEst {
		IdsEst = append(IdsEst, id.Hex())
	}
	return &productmodel.Product{
		ID:       p.ID.Hex(),
		IDCl:     p.ID.Hex(),
		IDsEst:   IdsEst,
		Name:     p.Name,
		Category: p.Category,
		Price:    p.Price,
		Status:   p.Status,
	}
}

func toModelList(pArr []*Product) []*productmodel.Product {
	modelList := []*productmodel.Product{}
	for _, p := range pArr {
		pm := toModel(p)
		modelList = append(modelList, pm)
	}
	return modelList
}

func toProduct(pm *productmodel.Product) *Product {
	id, _ := primitive.ObjectIDFromHex(pm.ID)
	idCl, _ := primitive.ObjectIDFromHex(pm.IDCl)

	IdsEst := []primitive.ObjectID{}
	for _, idStr := range pm.IDsEst {
		idObj, _ := primitive.ObjectIDFromHex(idStr)
		IdsEst = append(IdsEst, idObj)
	}

	return &Product{
		ID:       id,
		IDCl:     idCl,
		IDsEst:   IdsEst,
		Name:     pm.Name,
		Category: pm.Category,
		Price:    pm.Price,
		Status:   pm.Status,
	}
}
