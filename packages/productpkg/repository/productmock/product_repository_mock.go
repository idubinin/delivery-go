package productmock

import (
	"context"
	"goapp/bin/testdata"
	"goapp/models/productmodel"
)

// ProductRepositoryMock mock for productpkg.Repository
type ProductRepositoryMock struct{}

// GetProducts contain requests to get list of products
func (r ProductRepositoryMock) GetProducts(ctx context.Context) ([]*productmodel.Product, error) {
	pmArr := testdata.GetTestProducts()
	pmLinksArr := make([]*productmodel.Product, len(pmArr))

	for i, pmVal := range pmArr {
		pmLinksArr[i] = &pmVal
	}

	return pmLinksArr, nil
}

// GetProduct - detail of product
func (r ProductRepositoryMock) GetProduct(ctx context.Context, id string) (productmodel.Product, error) {
	pmArr := testdata.GetTestProducts()
	pm := productmodel.Product{}

	for _, p := range pmArr {
		if p.ID == id {
			pm = p
			break
		}
	}

	return pm, nil
}
