package clientpkg

import (
	"context"
	"goapp/models/clientmodel"
	"goapp/models/usermodel"
)

type Repository interface {
	CreateClient(ctx context.Context, user *usermodel.UserSystem, client *clientmodel.Client) error
	// GetClient(ctx context.Context, user *models.User) ([]*models.Client, error)
	// DeleteClient(ctx context.Context, user *models.User, id string) error
}
