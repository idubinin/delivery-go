package clientpkg

import (
	"context"
	"goapp/models/usermodel"
)

type UseCase interface {
	CreateClient(ctx context.Context, customer *usermodel.UserSystem, name, email, phoneNumber string) error
	// GetClient(ctx context.Context, user *models.User) ([]*models.Client, error)
	// DeleteClient(ctx context.Context, user *models.User, id string) error
}
