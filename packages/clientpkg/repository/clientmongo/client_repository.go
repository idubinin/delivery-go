package clientmongo

import (
	"context"
	"goapp/models/clientmodel"
	"goapp/models/usermodel"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type ClietnRepository struct {
	clientCollection *mongo.Collection
}

func NewClientRepository(db *mongo.Database) *ClietnRepository {
	return &ClietnRepository{
		clientCollection: db.Collection("clients"),
	}
}

func (r ClietnRepository) CreateClient(ctx context.Context, user *usermodel.UserSystem, cm *clientmodel.Client) error {
	cr := toClient(cm)

	res, err := r.clientCollection.InsertOne(ctx, cr)
	if err != nil {
		return err
	}

	cm.ID = res.InsertedID.(primitive.ObjectID).Hex()
	return nil
}

/*


type BookmarkRepository struct {
	db *mongo.Collection
}

func NewBookmarkRepository(db *mongo.Database, collection string) *BookmarkRepository {
	return &BookmarkRepository{
		db: db.Collection(collection),
	}
}

func (r BookmarkRepository) CreateBookmark(ctx context.Context, user *models.User, bm *models.Bookmark) error {
	bm.UserID = user.ID

	model := toModel(bm)

	res, err := r.db.InsertOne(ctx, model)
	if err != nil {
		return err
	}

	bm.ID = res.InsertedID.(primitive.ObjectID).Hex()
	return nil
}



func toModel(b *models.Bookmark) *Bookmark {
	uID, _ := primitive.ObjectIDFromHex(b.UserID)

	return &Bookmark{
		UserID: uID,
		URL:    b.URL,
		Title:  b.Title,
	}
}

func toBookmark(b *Bookmark) *models.Bookmark {
	return &models.Bookmark{
		ID:     b.ID.Hex(),
		UserID: b.UserID.Hex(),
		URL:    b.URL,
		Title:  b.Title,
	}
}

func toBookmarks(bs []*Bookmark) []*models.Bookmark {
	out := make([]*models.Bookmark, len(bs))

	for i, b := range bs {
		out[i] = toBookmark(b)
	}

	return out
}

*/
