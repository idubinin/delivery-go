package clientmongo

import (
	"goapp/models/clientmodel"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Establishment struct {
	ID          primitive.ObjectID `bson:"_id,omitempty"`
	IDCl        primitive.ObjectID `bson:"_id_client"`
	Name        string             `bson:"name"`
	Email       string             `bson:"email"`
	PhoneNumber string             `bson:"phone_number"`
}

func toModelEstablishment(e *Establishment) *clientmodel.Establishment {
	return &clientmodel.Establishment{
		ID:          e.ID.Hex(),
		IDCl:        e.ID.Hex(),
		Name:        e.Name,
		Email:       e.Email,
		PhoneNumber: e.PhoneNumber,
	}
}

func toEstablishment(em *clientmodel.Establishment) *Establishment {
	ID, _ := primitive.ObjectIDFromHex(em.ID)
	clientID, _ := primitive.ObjectIDFromHex(em.IDCl)

	return &Establishment{
		ID:          ID,
		IDCl:        clientID,
		Name:        em.Name,
		Email:       em.Email,
		PhoneNumber: em.PhoneNumber,
	}
}
