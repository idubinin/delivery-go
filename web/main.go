package main

import (
	"goapp/packages/productpkg"
	"goapp/packages/productpkg/productcase"
	"goapp/packages/productpkg/repository/productmongo"
	"goapp/packages/userpkg"
	"goapp/packages/userpkg/repository/usermongo"
	"goapp/packages/userpkg/usercase"
	"goapp/utils"
	"goapp/web/webgraphql"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/graphql-go/handler"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
)

func getHandler(db *mongo.Database) gin.HandlerFunc {
	type UseCases struct {
		usercase    userpkg.UseCase
		productcase productpkg.UseCase
	}

	userRepo := usermongo.NewUserRepository(db, "users_system")
	productRepo := productmongo.NewProductRepository(db, "products")

	uc := UseCases{
		usercase: usercase.NewAuthUseCase(
			userRepo,
			viper.GetString("app.hash_salt"),
			[]byte(viper.GetString("app.signing_key")),
			viper.GetDuration("app.token_ttl"),
		),
		productcase: productcase.NewProductCase(productRepo),
	}

	schema, _ := webgraphql.GetSchema(&uc.productcase, &uc.usercase)

	return func(c *gin.Context) {
		// Creates a GraphQL-go HTTP handler with the defined schema
		h := handler.New(&handler.Config{
			Schema:   &schema,
			Pretty:   true,
			GraphiQL: true,
		})

		h.ServeHTTP(c.Writer, c.Request)
	}
}

func getUseCases(db *mongo.Database) {

}

func main() {
	if err := utils.CfgInit(); err != nil {
		log.Fatalf("%s", err.Error())
	}

	mongoDb := utils.MongoInit("")
	graphHandler := getHandler(mongoDb)

	r := gin.Default()
	r.Use(
		gin.Recovery(),
		gin.Logger(),
	)

	r.GET("/graphql", graphHandler)
	r.POST("/graphql", graphHandler)

	// Listen and serve on 0.0.0.0:8080
	r.Run(":" + viper.GetString("app.port"))

}
