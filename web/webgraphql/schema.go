package webgraphql

import (
	"goapp/packages/productpkg"
	"goapp/packages/userpkg"
	"goapp/web/webgraphql/webtypes/producttype"
	"goapp/web/webgraphql/webtypes/usertype"

	"github.com/graphql-go/graphql"
)

// GetSchema - return graphql schema for http requests
func GetSchema(productcase *productpkg.UseCase, usercase *userpkg.UseCase) (graphql.Schema, error) {
	fields := graphql.Fields{
		"products": &graphql.Field{
			Type: graphql.NewList(producttype.ProductType),
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				return (*productcase).GetProducts(p.Context)
			},
		},
		"product": &graphql.Field{
			Type: producttype.ProductType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				id, ok := p.Args["id"].(string)
				if !ok {
					return nil, productpkg.ErrProductNotFound
				}
				return (*productcase).GetProduct(p.Context, id)
			},
		},

		"signIn": &graphql.Field{
			Type: usertype.SignInType,
			Args: graphql.FieldConfigArgument{
				"login": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"password": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				login, ok := p.Args["login"].(string)
				if !ok {
					return nil, userpkg.ErrUserNotFound
				}
				password, ok := p.Args["password"].(string)
				if !ok {
					return nil, userpkg.ErrUserNotFound
				}

				token, err := (*usercase).SignIn(p.Context, login, password)
				if err != nil {
					return nil, err
				}

				res := make(map[string]string)
				res["token"] = token
				return res, nil
			},
		},

		"parseToken": &graphql.Field{
			Type: usertype.UserType,
			Args: graphql.FieldConfigArgument{
				"token": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				token, ok := p.Args["token"].(string)
				if !ok {
					return nil, userpkg.ErrInvalidAccessToken
				}

				user, err := (*usercase).ParseToken(p.Context, token)
				if err != nil {
					return nil, err
				}

				return user, err
			},
		},
	}

	rootQuery := graphql.ObjectConfig{
		Name:   "RootQuery",
		Fields: fields,
	}

	schemaConfig := graphql.SchemaConfig{
		Query: graphql.NewObject(rootQuery),
	}

	return graphql.NewSchema(schemaConfig)
}
