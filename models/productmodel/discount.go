package productmodel

// Discount is the entity of discount
type Discount struct {
	ID         string
	IDCl       string
	IDEst      string
	IDsProd    []string
	IDsDisc    []string
	Name       string
	Status     string
	Type       string
	Value      int
	DateStart  string
	DateFinish string
	DateCreate string
}
