package productmodel

// Order is entity of order
type Order struct {
	ID      string
	IDCl    string
	IDEst   string
	IDCust  string
	IDsProd []string
	IdsDisc []string
	Name    string
	Price   int
	Date    string
	Status  string
}
