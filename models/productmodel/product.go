package productmodel

// Product is the entity of product
type Product struct {
	ID       string
	IDCl     string
	IDsEst   []string
	Name     string
	Category string
	Price    int
	Status   string
}
