package productmodel

// OrderLog is entity of history of events from order live circle
type OrderLog struct {
	ID     string
	IDOrd  string
	IDUsr  string
	Date   string
	Status string
}
