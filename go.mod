module goapp

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.5.0
	github.com/graphql-go/graphql v0.7.9
	github.com/graphql-go/handler v0.2.3
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.5.1
	go.mongodb.org/mongo-driver v1.3.1
)
