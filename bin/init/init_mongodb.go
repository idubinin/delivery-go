package main

import (
	// "goapp/models/clientmodel"
	"goapp/bin/testdata"
	"goapp/utils"
	"log"
)

func main() {
	if err := utils.CfgInit(); err != nil {
		log.Fatalf("%s", err.Error())
	}
	db := utils.MongoInit("delivery")

	testdata.InsertClient(db, "clients")
	testdata.InsertEstablishments(db, "establishments")
	testdata.InsertProducts(db, "products")
	testdata.InsertUsers(db, "users_system")
}
