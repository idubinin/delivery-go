package testdata

import "goapp/models/productmodel"

// GetTestProducts return data of test products
func GetTestProducts() []productmodel.Product {
	TestProducts := []productmodel.Product{
		productmodel.Product{
			ID:       "5e874f4b327272d07e537a4d",
			IDCl:     "5e874f4b327272d07e537a44",
			IDsEst:   []string{"5e874f4b327272d07e537a46"},
			Name:     "Steak New York",
			Category: "steak",
			Price:    200,
			Status:   "active",
		},
		productmodel.Product{
			ID:       "5e874f4b327272d07e537a4c",
			IDCl:     "5e874f4b327272d07e537a44",
			IDsEst:   []string{"5e874f4b327272d07e537a46"},
			Name:     "Red wine",
			Category: "drinks",
			Price:    150,
			Status:   "active",
		},
		productmodel.Product{
			ID:       "5e874f4b327272d07e537a4b",
			IDCl:     "5e874f4b327272d07e537a44",
			IDsEst:   []string{"5e874f4b327272d07e537a46"},
			Name:     "Salad Cesar",
			Category: "salad",
			Price:    100,
			Status:   "active",
		},
		productmodel.Product{
			ID:       "5e874f4b327272d07e537a4a",
			IDCl:     "5e874f4b327272d07e537a44",
			IDsEst:   []string{"5e874f4b327272d07e537a45"},
			Name:     "Cola",
			Category: "drinks",
			Price:    50,
			Status:   "active",
		},
		productmodel.Product{
			ID:       "5e874f4b327272d07e537a49",
			IDCl:     "5e874f4b327272d07e537a44",
			IDsEst:   []string{"5e874f4b327272d07e537a45"},
			Name:     "Chicken Barbecue",
			Category: "pizza",
			Price:    200,
			Status:   "active",
		},
		productmodel.Product{
			ID:       "5e874f4b327272d07e537a48",
			IDCl:     "5e874f4b327272d07e537a44",
			IDsEst:   []string{"5e874f4b327272d07e537a45"},
			Name:     "Four Cheese",
			Category: "pizza",
			Price:    150,
			Status:   "active",
		},
		productmodel.Product{
			ID:       "5e874f4b327272d07e537a47",
			IDCl:     "5e874f4b327272d07e537a44",
			IDsEst:   []string{"5e874f4b327272d07e537a45"},
			Name:     "Pepperoni",
			Category: "pizza",
			Price:    100,
			Status:   "active",
		},
	}

	return TestProducts
}
